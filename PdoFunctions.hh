<?hh //strict
/*****************************************************************************
 *                                                                           *
 * This file is part of the Nuclio framework.                                *
 *                                                                           *
 * (c) Zinios <support@zinios.com>                                           *
 *                                                                           *
 * For the full copyright and license information, please view the LICENSE   *
 * file that was distributed with this source code.                          *
 *                                                                           *
 *****************************************************************************/
namespace nuclio\plugin\database\common
{
	use nuclio\plugin\provider\manager\Manager as ProviderManager;
	use nuclio\plugin\database\
	{
		common\CommonInterface,
		common\CommonCursorInterface,
		common\DBRecord,
		common\DBVector,
		common\DBQuery,
		common\DBFields,
		common\DBCondition,
		orm\ORM,
		orm\Model,
		queryBuilder\QueryBuilder,
		exception\DatabaseException
	};
	use \PDO;
	use \PDOStatement;
	use \PDOException;
	
	abstract class PdoFunctions implements CommonInterface
	{
		const DEFAULT_LIMIT=1000000;
		
		public ?PDO $connection;
		public ?PDOStatement $pdoStatement;
		public ?bool $bConnected=false;
		
		abstract public function collectionExists(string $target):bool;
		abstract public function createCollection(Model $model, array<mixed,mixed> $options=[]):bool;
		
		public function query(string $query, Vector<mixed> $params=Vector{}):CommonCursorInterface
		{
			$statement=$this->connection->prepare($query);
			for ($i=0,$j=count($params); $i<$j; $i++)
			{
				$value=$params->get($i);
				if ($value instanceof Model)
				{
					$statement->bindValue($i+1,$value->getId());
				}
				else
				{
					$statement->bindValue($i+1,$value);
				}
			}
			$statement->execute();
			
			$cursor=ProviderManager::request('database::mysql::cursor',$statement);
			if ($cursor instanceof CommonCursorInterface)
			{
				return $cursor;
			}
			else
			{
				throw new Exception('Expected an instance of CommonCursorInterface but didn\'t receive one from the Provider Manager. Please check that you are using a compatible driver.');
			}
		}
		
		/**
		 * Make Query Using PDO
		 * @param  string $query define
		 * @return mixed result of query
		 */
		public function find(string $target, DBQuery $query, DBOptions $options=Map{}):CommonCursorInterface
		{
			$builder=QueryBuilder::getInstance(ORM::getInstance()->getDataSource(),'queryBuilder::mysql');
			$limit	=$options->get('limit') ?? self::DEFAULT_LIMIT;
			$offset	=$options->get('offset') ?? 0;
			$orderBy=$options->get('orderBy') ?? Map{};
			
			$builder->setTarget($target)
					->setFilter($query)
					->setLimit($limit)
					->setOffset($offset)
					->setOrderBy($orderBy);
			
			try
			{
				$cursor=$builder->execute();
			}
			catch (DatabaseException $exception)
			{
				$debug=$exception->getDebug();
				$debug->set('options',$options);
				throw $exception;
			}
			catch (\Exception $exception)
			{
				//Upgrade to DatabaseException.
				$exception=new DatabaseException($exception->getMessage());
				$exception->setDebug
				(
					Map
					{
						'query'		=>$builder->getLastQuery(),
						'options'	=>$options
					}
				);
				throw $exception;
			}
			
			// var_dump($cursor instanceof CommonCursorInterface);
			// exit();
			if ($cursor instanceof CommonCursorInterface)
			{
				return $cursor;
			}
			else
			{
				throw new \Exception('Expected an instance of CommonCursorInterface but didn\'t receive one the Query Builder.');
			}
		}
		
		public function findOne(string $target, DBQuery $query, DBOptions $options=Map{}):?DBRecord
		{
			$options->set('limit',1);
			$result	=$this->find($target,$query,$options);
			$current=$result->current();
			if (!is_null($current))
			{
				return $current->toMap();
			}
			return null;
		}
		
		
		public function findById(string $target, mixed $id, DBOptions $options=Map{}):?DBRecord
		{
			return $this->findOne($target,Map{'id'=>$id},$options);
		}
		
		/**
		 * Count Colum of query result.
		 * @param  String $sql
		 * @return Mixed if yes return int columCount else return false.
		 */
		public function columnCount(string $target, string $sql):mixed
		{
			if (!is_null($this->pdo))
			{
				try
				{
					$result=$this->pdo->query($sql);
					 if($result	!==false)
					{
						$cols=$result->columnCount();
						return $cols;
					}
					else
					{
						return	false;
					}
				}
				catch (PDOException $e)
				{
					throw new DatabaseException($e->getMessage());
				}
			}
			else
			{
				throw new DatabaseException('PDO IS NULL!');
			}
		}
		
		/**
		 * Prepare Query For PDO
		 * @param  String $Query
		 * @param array $parameter [define prepare parameters]
		 * @return Mixed return all rows or false
		 */
		public function prepare(string $target, string $query, array<string,string> $parameter):mixed
		{
			$result = null;
			$fetchresult = array();
		 	if (!is_null($this->pdo))
			{
				try
				{
					$this->pdoStatement=$this->pdo->prepare($query);
					if (!is_null($this->pdoStatement))
					{
						if (!is_null($this->pdoStatement) && $this->pdoStatement->execute($parameter))
						{
							if (!is_null($this->pdoStatement) && ($result = $this->pdoStatement->fetchAll()))
							{
								if ($result!==null)
								{
									$fetchresult=$result;
									return $fetchresult;
								}
								else
								{
									return	false;
								}
							}
						}
					}
					else
					{
						throw new DatabaseException('PDO Statement is Null!');
					}
				}
				catch(PDOException $e)
				{
					throw new DatabaseException($e->getMessage());
				}
			}
			else
			{
				throw new DatabaseException('PDO IS NULL!');
			}
		}
		
		/**
		 * Execute Just Run Query
		 * @param  String $sql define query
		 * @return Mixed Return rows or false
		 */
		public function exec(string $target, string $sql):mixed
		{
			if (!is_null($this->pdo))
			{
				try
				{
					$result=$this->pdo->exec($sql);
					if ($result!==false)
					{
						return $result;
					}
					else
					{
						return false;
					}
				}
				catch (PDOException $e)
				{
					throw new DatabaseException($e->getMessage());
				}
			}
			 else
			{
				throw new DatabaseException('PDO IS NULL!');
			}
		}

		public function beginTransaction():void
		{

		}

		public function commit():void
		{

		}

		public function rollBack():void
		{

		}
		 
		/**
		 * Insert/Update New Row
		 *
		 * @param  String $table define table.
		 * @param  Array $fields define all fields in arrays
		 * @param  Array $values define all values in arrays
		 * @return Mixed Return rows or false
		 */
		public function upsert(string $target, DBRecord $record, DBQuery $condition=Map{}, DBOptions $options=Map{}):DBRecord
		{
			//Don't work with null
			if ($record->containsKey('id'))
			{
				$id=$record->get('id');
				if (is_null($id))
				{
					$record->remove('id');
				}
			}
			
			$keys				=array_keys($record);
			$values				=array_values($record);
			$valuePlaceholders	=Vector{};
			$columns			='`'.implode('`,`',$keys).'`';
			$update				=Vector{};
			
			foreach ($record as $key=>$value)
			{
				$valuePlaceholders->add(':'.$key);
				$update->add(sprintf('`%s`=:%s',$key,$key));
			}
			
			$valuePlaceholders=implode(',',$valuePlaceholders);
			$update=implode(',',$update);
			$query=<<<SQL
			INSERT INTO `{$target}`
			({$columns})
			VALUES
			({$valuePlaceholders})
			ON DUPLICATE KEY
			UPDATE {$update};
SQL;
			$statement=$this->connection->prepare($query);
			foreach ($record as $key=>$value)
			{
				if (is_array($value))
				{
					$map=new Map($value);
					if ($map->containsKey('_orm'))
					{
						$ORM=new Map($map->get('_orm'));
						//TODO: Look into using id instead of _id
						if ($ORM->containsKey('_id'))
						{
							$value=$ORM->get('_id');
						}
					}
				}
				else if ($value instanceof Model)
				{
					$value=$value->getId();
				}
				$statement->bindValue(':'.$key,$value);
			}
			try
			{
				$result=$statement->execute();
			}
			catch (\Exception $exception)
			{
				$exception=new DatabaseException($exception->getMessage());
				$exception->setDebug
				(
					Map
					{
						'query'	=>$query,
						'record'=>$record
					}
				);
				throw $exception;
			}
			if ($result)
			{
				$id=null;
				if ($record->containsKey('id'))
				{
					$id=$record->get('id');
				}
				if (!$id)
				{
					$id=$this->connection->lastInsertId();
				}
				$record->set('id',$id);
				return $record;
			}
			else
			{
				//TODO: Handle with a better exception message.
				throw new DatabaseException('Upsert Failed. And we need a better error message.');
			}
			
			return new Map(null);
		}
		
		public function listCollections():Vector<mixed>
		{
			return new Vector(null);
		}
		
		public function distinct(string $target, string $field):Vector<string>
		{
			return new Vector(null);
		}
		
		public function delete(string $target, DBQuery $query, bool $justOne=false):bool
		{
			$builder=QueryBuilder::getInstance(ORM::getInstance()->getDataSource(),'queryBuilder::mysql');
			$builder->setTarget($target)
					->setFilter($query);
			list($where,$params)=$builder->parseFilter();
			$query				=sprintf('DELETE FROM `%s` %s;',$target,$where);
			$statement			=$this->connection->prepare($query);
			for ($i=0,$j=count($params); $i<$j; $i++)
			{
				$value=$params->get($i);
				if (is_array($value))
				{
					$map=new Map($value);
					if ($map->containsKey('_orm'))
					{
						$ORM=new Map($map->get('_orm'));
						//TODO: Look into using id instead of _id
						if ($ORM->containsKey('_id'))
						{
							$value=$ORM->get('_id');
						}
					}
				}
				else if ($value instanceof Model)
				{
					$value=$value->getId();
				}
				$statement->bindValue($i+1,$value);
			}
			try
			{
				$statement->execute();
			}
			catch (\Exception $exception)
			{
				throw new DatabaseException($exception->getMessage());
			}
			return true;
		}
		
		public function listCollectionFields(string $collection):Vector<Pair<string,string>>
		{
			return new Vector(null);
		}
	}
}
